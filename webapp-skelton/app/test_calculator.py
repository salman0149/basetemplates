import pytest
from app import app

@pytest.fixture
def client():
    app.config['TESTING'] = True
    client = app.test_client()
    yield client

def test_index_page(client):
    response = client.get('/')
    assert response.status_code == 200

def test_addition(client):
    response = client.post('/calculate', data={'num1': '5', 'operation': 'add', 'num2': '3'})
    assert b"Result: 8.0" in response.data

def test_subtraction(client):
    response = client.post('/calculate', data={'num1': '10', 'operation': 'subtract', 'num2': '2'})
    assert b"Result: 8.0" in response.data

def test_multiplication(client):
    response = client.post('/calculate', data={'num1': '5', 'operation': 'multiply', 'num2': '4'})
    assert b"Result: 20.0" in response.data

def test_division(client):
    response = client.post('/calculate', data={'num1': '10', 'operation': 'divide', 'num2': '2'})
    assert b"Result: 5.0" in response.data

def test_division_by_zero(client):
    response = client.post('/calculate', data={'num1': '10', 'operation': 'divide', 'num2': '0'})
    assert b"Division by zero is not allowed." in response.data
